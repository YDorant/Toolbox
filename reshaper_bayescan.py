#!/usr/bin/env python3
"""convert vcf file to bayescan input format

Usage:
    <program> input_vcf popmap_file output_file

Output:
    bayescan output file
    blacklist of monomorphic snps
    whitelist of polymorphic snps
"""

# Modules
import sys, time

# Parse user input
try:
    input_vcf = sys.argv[1]
    popmap_file = sys.argv[2]
    output_file = sys.argv[3]
except:
    print(__doc__)
    sys.exit(0)


"""Script steps
    1- Parse population map and store each pop and their samples in dictionary
    2- Parse Vcf file - read it line by line :
            a. Check if population map have the same number of samples than VCF file
            b. Test if snp is monomorphic in global (i.e. uninformative)
                if is monomorphic --> store in monomorphic snp blacklist
                if is polymorphic --> store in polymorphic snp whitelist
            c. Extract allele count for each population and store in dictionary
    3- Print data informations
    4- Write bayescan output file
    5- Write blacklist monomorphic snps
    6- Write Whitelist polymorphic snps (ref_num is a link with bayescan file)
"""

##########################################################################
# Functions
##########################################################################
def update_progress(job_title, progress):
    length = 20 # modify this to change the length
    block = int(round(length*progress))
    msg = "\r{0}: [{1}] {2}%".format(job_title, "#"*block + "-"*(length-block), round(progress*100, 0))
    if progress >= 1: msg += " DONE\r\n"
    sys.stdout.write(msg)
    sys.stdout.flush()
    
def test_monomorph(genotypes_list):
    """Return True or false if snp is/not monomorph
    """
    n_total = len(genotypes_list)
    n_homo_r = genotypes_list.count('0/0')
    n_hetero =  len([genotypes_list.count(i) for i in genotypes_list if i in ['0/1', '1/0']])
    n_homo_a = genotypes_list.count('1/1')
    if n_total == n_homo_r or n_total == n_hetero or n_total == n_homo_a:
        return True
    else:
        return False


##########################################################################
# Main
##########################################################################

# Read popmap and create popmap dictionary
with open(popmap_file) as popfile:
    
    #define popmap dictionary
    pop_dic={}

    # Iterate over lines
    for line in popfile:
        l = line.strip().split("\t")

        # Get pop info
        ind, pop  = l[:2]
        
        #Fill pop_dict - test if pop already exist
        if pop in pop_dic:
            pop_dic[pop] = pop_dic[pop] + [ind]
        else:
            pop_dic[pop] = [ind]
popfile.close()

#Get number of populations - Control
npop = len(pop_dic.keys())
#Get number of samples per population - Control
ind_count_list = [len(value) for pop,value in pop_dic.items()]

#Get number of lines in the input vcf (need for progress bar)
num_lines = sum(1 for line in open(input_vcf))

# Read vcf file 
with open(input_vcf) as vcffile:

        #define storage variables
        geno_dict = dict()
        snps_monomorph_list = []
        snps_polymorph_list = []
        #Define SNP count variable
        snp_count = 0
        
        # Iterate over loci and SNPs
        for line in vcffile:
            l = line.strip().split("\t")

            #Manage update progress bar
            try:
                i_progress
            except:
                i_progress = None
            
            if i_progress is None:
                i_progress=1
            else:
                update_progress("Parsing vcf...", (i_progress/(num_lines)*100)/100)

            # Pass header lines in VCF file
            if line.startswith("##"):
                i_progress =  i_progress +1
                continue

            #Control for number of samples between popmap and vcf parsed
            if line.startswith("#CHROM"):
                if len(l[9:]) == sum(x for x in ind_count_list):
                    continue
                else:
                    print("Error: The number of samples in population map is not the same found in the vcf")
                    sys.exit(1)

            # Get locus information
            scaffold, position = l[:2]
            
            #Get snp genotypes
            snp_genotypes = [str(i) for i in [x.split(":")[0] for x in l[9:] 
                                if x.split(":")[0] != "./."]]
            
            #test if the snp is monomorphic
            if test_monomorph(snp_genotypes):

                #add info snps_monomorph_list
                snps_monomorph_list.append([scaffold, position])
                #sys.exit(1)
            else:
                #add info snps_polymorph_list
                snps_polymorph_list.append([scaffold,position])

                # update locus count
                snp_count = snp_count +1  
                # Get genotypes data per pop
                iterator_left = 9
                for pop in range(0,npop):
                    #define/update iterator_right for population sliding window
                    iterator_right = iterator_left + ind_count_list[pop]
    
                    #get genotypes data for the pop i
                    data_pop = [[int(i[0]), int(i[1])] for i in [x.split(":")[0].split("/") 
                        for x in l[iterator_left:iterator_right] if x.split(":")[0] in ["0/0", "1/1","0/1", "1/0"]]]
                    #print(data_pop)
                    data_pop1 = [val for sublist in data_pop for val in sublist]
                    #print(data_pop1)
                    # add in dictionary
                    if list(pop_dic.keys())[pop] in geno_dict:
                        geno_dict[list(pop_dic.keys())[pop]] = geno_dict[list(pop_dic.keys())[pop]] + [[data_pop1.count(0),data_pop1.count(1)]]
                    else :
                        geno_dict[list(pop_dic.keys())[pop]] = [[data_pop1.count(0),data_pop1.count(1)]]
    
                    #update iterator_left for population sliding window
                    iterator_left = iterator_left + ind_count_list[pop]
            #Increment progress bar by one
            i_progress =  i_progress +1

# Print data informations
print("\n#########################################")
print("Number of populations\t\t|",npop)
print("Total number of snps found \t|", len(snps_polymorph_list) + len(snps_monomorph_list))
print("Number of monomorphic snps \t|", len(snps_monomorph_list))
print("Number of polymorphic snps \t|", len(snps_polymorph_list))
print("\nNumber of samples per population:\n",ind_count_list)
print("#########################################")


# Write bayescan output file
with open(output_file, "w") as outfile:
    outfile.write("[loci]="+str(snp_count) + "\n\n" + "[populations]=" + str(npop) +"\n\n")
    for pop in range(0,npop):
        #print("[pop]=",pop+1)
        outfile.write("\n" + "[pop]=" + str(pop+1) + "\n")
        for i in range(0,snp_count):
            snp_info = [
                    int(i+1),
                    sum(geno_dict.get(str(list(pop_dic.keys())[pop]))[i]),
                    2,
                    geno_dict.get(str(list(pop_dic.keys())[pop]))[int(i)][0],
                    geno_dict.get(str(list(pop_dic.keys())[pop]))[int(i)][1],
                    ]
            info = [str(x) for x in snp_info]
            #print(info)
            #print('\t'.join(str(p) for p in info))
            outfile.write("\t".join(info) + "\n")
outfile.close()
    
# Write blacklist file of monomorphic loci
with open("blacklist_monomorphic_snps.txt", "w") as outfile:
    outfile.write("Locus" + "\t" + "Position" + "\n")
    for snp in snps_monomorph_list:
        outfile.write("\t".join(snp) + "\n")
outfile.close()

# Write Whitelist file of polymorphic loci
with open("whitelist_polymorphic_snps.txt", "w") as outfile:
    outfile.write("Ref_num" + "\t" + "Locus" + "\t" + "Position" + "\n")
    for i in range(0,snp_count):
        outfile.write(str(i+1) + "\t" + "\t".join(snps_polymorph_list[i]) + "\n")
outfile.close()
