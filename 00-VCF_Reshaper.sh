#!/usr/bin/bash

#================================================================
# HEADER
#================================================================
#% TITLE
#%    vcf Reshaper
#%
#% SYNOPSIS
#+    ${SCRIPT_NAME} [-h] [-o[file]] args ...
#%
#% DESCRIPTION
#%    This script convert VCF file in specific geno format
#%    using both vcftools, Python and Plink
#%
#% OPTIONS
#%    -i [file],           Parse input .vcf file
#%    -o,                  Set output file prefix (default=output)
#%    -p [file],           Parse population map file (Samples Populations)
#%    -f,                  Set output format (genepop, baypass, StAMPP, etc...)
#%    -m [value],          Minor allele frequency threshold
#%    -h, --help           Print help
#%
#% EXAMPLES
#%  bash 00-VCF_Reshaper.sh -v file.vcf -o output -p population_map.txt -f format
#%
#================================================================
#- IMPLEMENTATION
#-    version         0.2
#-    author          Yann Dorant
#-    Date            2018-06-29
#-    Latest update   2020-06-10
#-    Notes           Dependencies: vcftools; Python v.3.0+ (libs Pandas); Plink 1.9+
#================================================================


USAGE="Usage: bash 00-VCF_Reshaper.sh -v file.vcf -o output_name -p population_map.txt -f genepop"
VCF="NULL"
OUTPUT="NULL"
POPMAP="NULL"
FORMAT="NULL"
MAF=0

# --- Options processing -------------------------------------------
if [ $# == 0 ] ; then
    echo $USAGE
    exit 1;
fi
echo "###########################"
echo "###     Args called     ###"
echo "###########################"
while getopts ":v:o:p:f:m:h" optname
  do
    case "$optname" in
      "v")
        echo "vcf file : -v $OPTARG"
        VCF=$OPTARG
        ;;
      "o")
        echo "Output prefix : -o $OPTARG"
        OUTPUT=$OPTARG
        ;;
      "p")
        echo "Population map file : -p $OPTARG"
        POPMAP=$OPTARG
        ;;
      "f")
        echo "Format output : -f $OPTARG"
        FORMAT=$OPTARG
        ;;
        "m")
        echo "maf filtering : -m $OPTARG"
        MAF=$OPTARG
        ;;
      "h")
        echo $USAGE
        printf "Available output format:"
        printf "\n -genepop\n -baypass\n -bayescan\n -bayenv\n -StAMPP\n -assignPOP\n -conStruct\n"
        exit 0;
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 0;
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 0;
        ;;
      *)
        echo "Unknown error while processing options"
        exit 0;
        ;;
    esac
  done
echo -e "###########################\n"
#control args
if [ $VCF == "NULL" ] ||
   [ $POPMAP == "NULL" ] ||
   [ $OUTPUT == "NULL" ] ||
   [ $FORMAT == "NULL" ]
then
    echo "Error: 4 arguments are required"
    echo $USAGE
    exit 1;
fi

#================================================================
#                      Functions
#================================================================

make_genepop() {
    echo "make genepop..."
    #Run vcftools
    printf "\n-----STEP 1-----\nRun vcftools\n"
    vcftools --vcf $VCF --keep $POPMAP --012 --out $OUTPUT --maf $MAF
    #test if popmap correspond to samples within the VCF file
    error_PopMap_Fail
    #Create geno file 
    printf "\n-----STEP 2-----\nCreate geno_file\n"
    paste $POPMAP <(cut -f 2- $OUTPUT.012) > geno_file.tmp
    #Run python script
    printf "\n-----STEP 3-----\nRun python script\n\n"
    joke

    python reshaper_genepop.py geno_file.tmp $OUTPUT.012.pos $OUTPUT
    echo "Genepop output file contains $(grep "Pop" $OUTPUT.genepop | wc -l) populations"

#remove temp files
rm *.012* *.log *.tmp
}

make_baypass() {
    echo "make baypass..."
    #Run python script reshaper_baypass.py
    python reshaper_baypass.py $VCF $POPMAP $OUTPUT
}

make_StAMPP() {
    echo "make StAMPP file..."
    #Run vcftools
    printf "\n-----STEP 1-----\nRun vcftools\n"
    vcftools --vcf $VCF --keep $POPMAP --012 --out $OUTPUT --maf $MAF
    #test if popmap correspond to samples within the VCF file
    error_PopMap_Fail
    #Create geno file 
    printf "\n-----STEP 2-----\nCreate StAMPP_file\n"
    awk '{ print $1"_"$2}' $OUTPUT.012.pos | perl -pe 's/\n/\t/g' > headerRight.tmp
    awk 'OFS="\t" {print $1,$2, 2, "BiA"}' $POPMAP > ploidy.tmp
    cut -f 2-  $OUTPUT.012             |
                  perl -pe 's/-1/-9/g'|
                  perl -pe 's/1/AB/g' | 
                  perl -pe 's/2/BB/g' |
                  perl -pe 's/0/AA/g' >  data.geno.tmp
    paste ploidy.tmp data.geno.tmp > data.geno2.tmp
    echo "Ind" "Pops" "Ploid" "Format" > headerLeft.tmp
    paste headerLeft.tmp headerRight.tmp > header.tmp
    cat header.tmp data.geno2.tmp > $OUTPUT.StAMPP

    rm *.012* *.tmp
}

make_conStruct() {
    echo "make geno file input for conStruct (Bradburd et al 2018)..."
    #Run plink 1.9
    printf "\n-----STEP 1-----\nRun Plink\n"
    plink --vcf $VCF --family --freq --allow-extra-chr --out $OUTPUT
    #Create geno file 
    printf "\n------> geno file created as $OUTPUT.frq.strat\n"
}

make_assignPOP() {
    echo "make geno file input for assignPOP (R package)..."
    #Run vcftools
    printf "\n-----STEP 1-----\nRun vcftools\n"
    vcftools --vcf $VCF --keep $POPMAP --012 --maf 0 --out $OUTPUT
    vcftools --vcf $VCF --keep $POPMAP --maf 0 --kept-sites --out $OUTPUT
    #test if popmap correspond to samples within the VCF file
    error_PopMap_Fail
    #Create geno matrix
    printf "\n------> geno file created as $OUTPUT.geno.txt\n"
    cut -f 2- $OUTPUT.012 | sed 's/0/88\t99/g' |
                            sed 's/2/99\t88/g' |
                            sed 's/-1/99\t99/g'|
                            sed 's/1/50\t50/g' |
                            sed 's/88/1.0/g'   |
                            sed 's/99/0.0/g'   |
                            sed 's/50/0.5/g' > $OUTPUT.geno.txt
    #reshape list of snps
    printf "\n------> snps list created as $OUTPUT.snps.list.txt\n"
    cut -f 1 $OUTPUT.kept.sites | tail -n +2 |
                                awk 'OFS="\t" {print $1"_110",$1"_120"}' > $OUTPUT.snps.list.txt
    rm *.012* *kept.sites
}

make_bayescan() {
    echo "make geno file input for bayescan (Foll 2008)..."
    #Run reshaper_bayesscan.py
    joke
    python reshaper_bayescan.py $VCF $POPMAP $OUTPUT
    
}

make_bayenv() {
    echo "make SNP file input for bayenv (Gnther & Coop 2013)..."
    #Run reshaper_bayesscan.py
    joke
    python reshaper_bayenv.py $VCF $POPMAP $OUTPUT
    
}

error_PopMap_Fail()
{   
    D=$(grep -vf $OUTPUT.012.indv <(cut -f 1 $POPMAP))
    W=$(grep -vf $OUTPUT.012.indv $POPMAP | wc -l)
    if [ $W = 0 ] #here test if there is not unmatch samples between PopMap and vcf .012.indv file
    then
            echo 'Match!'
    else
            printf "\n--------------------------------------------------------------|"
            printf "\n(!) ERROR:\n"
            printf "ERROR CODE:\t 2\n"
            printf "\nERROR MESSAGE:\nSome samples ($W) given by the Popmap are absent in the VCF file.\n"
            printf "Below, this is a short list of confused samples...\n" 
            echo $D  | fmt -1 | head -5 #print only the 5 first bug samples
            printf "\n--------------------------------------------------------------|\n"
            exit 1;
    fi
}


joke() {
    printf " Stay here ! You won't have time for coffee...\n"
    printf " I'm not PGDSpider...\n"

    echo '       **************************'
    echo '    .*##*:*####***:::**###*:######*.'
    echo '   *##: .###*            *######:,##*'
    echo ' *##:  :####:             *####*.  :##:'
    echo '  *##,:########**********:,       :##:'
    echo '   .#########################*,  *#*'
    echo '     *#########################*##:'
    echo '       *##,        ..,,::**#####:'
    echo '        ,##*,*****,        *##*'
    echo '          *#########*########:'
    echo '            *##*:*******###*'
    echo '             .##*.    ,##*'
    echo '               :##*  *##,'
    echo '                 *####:'
    echo '                   :,'
}

#================================================================
#                      MAIN
#================================================================

if [ $FORMAT == "genepop" ]
then
    make_genepop
elif [ $FORMAT == "baypass" ]
then
    make_baypass
elif [ $FORMAT == "StAMPP" ]
then
    make_StAMPP
elif [ $FORMAT == "conStruct" ]
then
    make_conStruct
elif [ $FORMAT == "assignPOP" ]
then
    make_assignPOP
elif [ $FORMAT == "bayescan" ]
then
    make_bayescan
elif [ $FORMAT == "bayenv" ]
then
    make_bayenv
else
    echo "ERROR 1255 : Output format - please see README or -h option for available format"
fi