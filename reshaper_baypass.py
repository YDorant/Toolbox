#!/usr/bin/env python3
"""convert vcf file to baypass input format

Usage:
    <program> input_vcf popmap_file output_file

Output:
    Baypass output file
"""
#Modules
import sys
import time

# Parse user input
try:
    input_vcf = sys.argv[1]
    popmap_file=sys.argv[2]
    output_file = sys.argv[3]
except:
    print(__doc__)
    sys.exit(1)

"""Script steps
    1- Parse population map and store each pop and their samples in dictionary
    2- Parse Vcf file - read it line by line :
            a. Check if population map have the same number of samples than VCF file
            b. Extract allele count for each population and store in dictionary
    3- Print data informations
    4- Write Baypass output file
"""

##########################################################################
# Functions
##########################################################################
def update_progress(job_title, progress):
    length = 20 # modify this to change the length
    block = int(round(length*progress))
    msg = "\r{0}: [{1}] {2}%".format(job_title, "#"*block + "-"*(length-block), round(progress*100, 0))
    if progress >= 1: msg += " DONE\r\n"
    sys.stdout.write(msg)
    sys.stdout.flush()

##########################################################################
# Main
##########################################################################

# Read popmap and create popmap dictionary
with open(popmap_file) as popfile:
    
    #define popmap dictionary
    pop_dic={}

    # Iterate over lines
    for line in popfile:
        l = line.strip().split("\t")

        # Get pop info
        ind, pop  = l[:2]
        
        #Fill pop_dict - test if pop already exist
        if pop in pop_dic:
            pop_dic[pop] = pop_dic[pop] + [ind]
        else:
            pop_dic[pop] = [ind]
popfile.close()

#Get number of populations - Control
npop = len(pop_dic.keys())
#Get number of samples per population - Control
ind_count_list = [len(value) for pop,value in pop_dic.items()]

#Get number of lines in the input vcf (need for progress bar)
num_lines = sum(1 for line in open(input_vcf))


# Read vcf file 
with open(input_vcf) as vcffile:

        #define storage variables
        geno_dict = dict()
        snps_monomorph_list = []
        snps_polymorph_list = []
        #Define SNP count variable
        snp_count = 0
        
        # Iterate over loci and SNPs
        for line in vcffile:
            l = line.strip().split("\t")

            #Manage update progress bar
            try:
                i_progress
            except:
                i_progress = None
            
            if i_progress is None:
                i_progress=1
            else:
                update_progress("Parsing vcf...", (i_progress/(num_lines)*100)/100)

            # Pass header lines in VCF file
            if line.startswith("##"):
                i_progress =  i_progress +1
                continue

            #Control for number of samples between popmap and vcf parsed
            if line.startswith("#CHROM"):
                if len(l[9:]) == sum(x for x in ind_count_list):
                    continue
                else:
                    print("Error: The number of samples in population map is not the same found in the vcf")
                    sys.exit(1)

            # Get locus information
            scaffold, position = l[:2]
            
            #Get snp genotypes
            snp_genotypes = [str(i) for i in [x.split(":")[0] for x in l[9:] 
                                if x.split(":")[0] != "./."]]

            # update locus count
            snp_count = snp_count +1  
            # Get genotypes data per pop
            iterator_left = 9
            for pop in range(0,npop):
                #define/update iterator_right for population sliding window
                iterator_right = iterator_left + ind_count_list[pop]
            
                #get genotypes data for the pop i
                data_pop = [[int(i[0]), int(i[1])] for i in [x.split(":")[0].split("/") 
                    for x in l[iterator_left:iterator_right] if x.split(":")[0] in ["0/0", "1/1","0/1", "1/0"]]]
                #print(data_pop)
                data_pop1 = [val for sublist in data_pop for val in sublist]
                #print(data_pop1)
                # add in dictionary
                if list(pop_dic.keys())[pop] in geno_dict:
                    geno_dict[list(pop_dic.keys())[pop]] = geno_dict[list(pop_dic.keys())[pop]] + [[data_pop1.count(0),data_pop1.count(1)]]
                else :
                    geno_dict[list(pop_dic.keys())[pop]] = [[data_pop1.count(0),data_pop1.count(1)]]
            
                #update iterator_left for population sliding window
                iterator_left = iterator_left + ind_count_list[pop]
            #Increment progress bar by one
            i_progress =  i_progress +1
vcffile.close()

# Print data informations
print("\n#########################################")
print("Number of populations\t\t|",npop)
print("Total number of snps found \t|",snp_count)
print("\nNumber of samples per population:\n", \
            {key: len(value) for key, value in pop_dic.items()})
print("#########################################")
#print(geno_dict.keys())

# Write baypass output file
with open(output_file, "w") as outfile:
    for snp in range(0,snp_count):
        info = list(geno_dict.get(pop)[snp] for pop in geno_dict.keys())
        info_flat = [str(i) for sublist in info for i in sublist]
        outfile.write("\t".join(info_flat) + "\n")
outfile.close()
