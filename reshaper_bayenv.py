#!/bin/env python3
"""convert vcf file to bayenv SNP input format

Usage:
    <program> input_vcf popmap_file output_file

Output:
    bayenv SNP output file
"""

# Modules
import sys, os

# Parse user input
try:
    input_vcf = sys.argv[1]
    popmap_file = sys.argv[2]
    output_file = sys.argv[3]
except:
    print(__doc__)
    sys.exit(0)


"""Script steps
    1- Parse population map and store each pop x sample in a list
    2- Parse Vcf file - read it line by line :
            a. Check if population map have the same number of samples than VCF file
            b. Extract allele count for each population
    3- Print Populations Allele counts for each SNPs within two successive lines
"""

##########################################################################
# Functions
##########################################################################
def get_pop_genotypes(Data,Pop):
    """ extract genotype info for population x
    """
    return([[x for x in group] for group in Data if group[0][1] == Pop])


def allele_counter(Data):
    """count number of each allele within POP
       return list of counts [Allele A, Allele B]
    """
    list_of_geno = [[geno for geno in group][1] for group in Data]
    list_of_geno = [y for x in list_of_geno for y in x]
    #print(list_of_geno)
    allele_A_counts = list_of_geno.count('0')
    #print(allele_A)
    allele_B_counts = list_of_geno.count('1')
    #print(allele_B)
    return([allele_A_counts, allele_B_counts])


##########################################################################
# Main
##########################################################################

# Remove output file if it already exist (avoid successive append)
if os.path.exists(output_file):
    os.remove(output_file)
else:
    pass


# Read popmap and create Population Map list [ind, POP]
with open(popmap_file) as popfile:
    #define popmap list
    PopMap=[]
    # Iterate over lines
    for line in popfile:
        l = line.strip().split("\t")
        # Get pop info
        PopMap.append(l)
popfile.close()

#make list of populations
PopNames=sorted(list(set([[POP for POP in group][1] for group in PopMap])))

#prepare empty list of size N populations
list_allele_counts=[None] * len(PopNames)


# Read vcf file 
with open(input_vcf) as vcffile:

        # Iterate over loci and SNPs
        for line in vcffile:
            l = line.strip().split("\t")
            
            if line.startswith("##"): #pass header lines
                pass
            elif line.startswith("#CHROM") and len(l[9:]) != len(PopMap): #check corresponding sizes PopMap and vcf samples
                print("Error: The number of samples in population map is not the same found in the vcf")
                sys.exit(1)
            elif not line.startswith("#") :
                snp_genotypes = [i for i in [x.split(":")[0] for x in l[9:] ]] #extract genotypes
         
                #create global list
                PopMap_genotypes_list=[]
                list_allele_counts=[]

                for j in range(0,38):
                    PopMap_genotypes_list.append([PopMap[j],snp_genotypes[j]])
                #print(PopMap_genotypes_list[0:10])

                for k in range(len(PopNames)):
                    list_allele_counts.append(allele_counter(get_pop_genotypes(PopMap_genotypes_list, PopNames[k])))
                #print(list_allele_counts)

                #write results line by line in output file
                with open(output_file, "a+") as outfile:
                    outfile.write("\t".join(map(str, [x[0] for x in list_allele_counts])) \
                                + "\n" \
                                + "\t".join(map(str, [x[1] for x in list_allele_counts])) \
                                + "\n")
                    outfile.close()
vcffile.close()