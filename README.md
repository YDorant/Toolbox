### -------------------------------------------------------------------------------
# Latest major update !


### bayenv input format for SNPs is now implemented ! (20 nov 2019)
### -------------------------------------------------------------------------------

# Tools scripts for genomics
### A script library for conversion and parsing VCF files.

#### author: Yann Dorant <yann.dorant.1@ulaval.ca>

#### license: Free


## overview

The [Variant Call Format (VCF)](http://www.1000genomes.org/wiki/Analysis/Variant%20Call%20Format/vcf-variant-call-format-version-41)
is a standard text file format of genomic data. It contains meta-information lines, a header line and then data lines each containing 
information genomic variations between individuals.
VCF is a usually format because it is unambiguous, scalable and flexible, allowing extra information to be added to the info field.
Thence, thousands to millions of genomic variants can be stored in a single VCF file generated, including so much individuals.

However many population genomic programs/tools use their own genomic data format and the most famous automated data conversion tool [PGDSpider2](http://www.cmpg.unibe.ch/software/PGDSpider/)
is struggling to convert very large NGS datasets (i.e. thousands of samples and thousands of markers).

For this reason, I decided to create a helpful tool in order to **fastly convert** relative big vcf files for some population genomic programs.
In addition, some other toolbox scripts were added to parse the genomic data.


## __Part 1. VCF conversion__
### usage
Conversion tool 00-VCF_Reshaper.sh provides a variety of conversion format for VCF files:

#### implementation
**00-VCF_Reshaper.sh** is implemented in bash and uses some dependences
* Python (v.3.0+) [libraries PANDAS]
* vcftools
* Plink (v.1.9+)

Some conversion formats need specific "children-scripts"
* **reshaper_genepop.py** for genepop format conversion
* **reshaper_baypass.py** for baypass format conversion
* **reshaper_bayescan.py** for bayescan format conversion
* **reshaper_bayenv.py** for bayenv format conversion

#### script execution
```bash 00-VCF_Reshaper.sh -v file.vcf -p population_map.txt -f format -o output_name```

**Note:** If you have the error message from vcftools "could not open temporary files", you have to allow your system for openning many temporary files. So use this command line to do this : ```ulimit -n (numberOfSamples+200)```
#### Population_map file
In order to reshape in many different genomic formats, you have to provide a population map file.
Use the same format as provided in the **examples_files folder**.

The first column **must** contains the **exact** name of the vcf samples names. The second column
must contains the group membership of each samples (e.g. sampling site, geographic region).

No header line in this file.

#### Available output formats
**Note:** respect the format name below

* **genepop** [Resource web](http://genepop.curtin.edu.au/help_input.html)
* **baypass** [Manual Baypass](http://www1.montpellier.inra.fr/CBGP/software/baypass/files/BayPass_manual_2.1.pdf)
* **StAMPP** [R resource](https://cran.r-project.org/web/packages/StAMPP/index.html)
* **conStruct** [R resource](https://cran.r-project.org/web/packages/conStruct/index.html)
* **assignPOP** [R resource](https://cran.r-project.org/web/packages/assignPOP/assignPOP.pdf)
* **bayescan** [Manual bayescan](http://cmpg.unibe.ch/software/BayeScan/files/BayeScan2.1_manual.pdf)
* **bayenv** [Manual bayenv](https://bitbucket.org/tguenther/bayenv2_public/src/default/)

**NOTE** : bayescan conversion tool exclude monomorphic markers (in global).

The Output file is escorted by a blacklist of monomorphic markers removed and a whitelist of markers kept in the output file.

## __Part 2. Toolbox scripts__

__StAMPP-fst.R__ Description : Rscript for pairwise FST calculation
- Requirements : R libraries [StAMPP ; dplyr ; magrittr ; tibble]
- USAGE : ```Rscript StAMPP-fst.R input.stampp output nCPU```

__conStruct.R__ Description : Rscript for modeling continuous and discrete population genetic structure (spatial model only).

Reference paper: Bradburd, G. S., Coop, G. M., & Ralph, P. L. (2018). Inferring Continuous and Discrete Population Genetic Structure Across Space. Genetics.
- Requirements : R libraries [conStruct ; dplyr ; magrittr ; tibble ; Reshape2]
- Input files : conStruct-geno format; sample-coordinates(i.e. columns=IDs,LAT,LON); distance.matrix(i.e. pairwise geographic distance matrix)
- USAGE only one K : ```Rscript conStruct.R [plink.frq.strat] [sample_coordinates] [distance.matrix] [number.of.MCMC.iterations] [number.of.chains] [K]```
- USAGE 2 to 8 K in parallel : ```seq 1 8 | parallel -j nCPU Rscript conStruct.R [plink.frq.strat] [sample_coordinates] [distance.matrix] [number.of.MCMC.iterations] [number.of.chains] {}```
 
__assignPOP.R__ Description : Rscript for Population Assignment using Genetic, Non-Genetic or Integrated Data in a Machine-learning Framework

Reference paper: Chen K-Y, Marschall EA, Sovic MG, etal. (2018). assignPOP: An R package for population assignment using genetic, non-genetic, or integrated data in a machine-learning framework. Methods in Ecology and Evolution. https://doi.org/10.1111/2041-210X.12897

Gitlab project: https://github.com/alexkychen/assignPOP

Tutorial: http://alexkychen.github.io/assignPOP/

- Requirements: R libraries [assignPOP ; dplyr ; magrittr ; tibble ; ggplot2]
- Input files: assignPOP.genofile; population.map; assignPOP.snps.list
- Output: Rplot assignment results
- Implemented only with Monte-Carlo cross-validation
- USAGE: ```Rscript assignPOP.R [assignPOP.genofile] [population.map] [assignPOP.snps.list] [nCPU] [output.prefix]```
- NOTE: change your assignment parameter directly in the .R script [assign.MC function]
