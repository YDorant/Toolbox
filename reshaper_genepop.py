#!/usr/bin/env python
"""
    child script called by 00-VCF_Reshaper.sh
    -> Use tmp geno matrix to output genepop file
    
Usage:
    ./script.py geno_file loci_file output_prefix

"""

import sys
import os
import pandas as pd


# Parse user input
try:
    input_geno_file = sys.argv[1]
    input_loci_file = sys.argv[2]
    output_file = sys.argv[3]+".genepop"

except:
    print(__doc__)
    sys.exit(1)


############ functions

def reshape_loci(list_genotypes):
    """change loci format
       -1 -> 000000
        0 -> 110110
        1 -> 110120
        2 -> 120120
    """
    for i in range (2,len(list_genotypes)):
        if list_genotypes[i] == '-1':
            list_genotypes[i] = '000000'
        elif list_genotypes[i] == '0':
            list_genotypes[i] = '110110'
        elif list_genotypes[i] == '1':
            list_genotypes[i] = '110120'
        elif list_genotypes[i] == '2':
            list_genotypes[i] = '120120'
        else:
            print("Error one genotype has error value in 012 file")
            sys.exit(1)
    return([list_genotypes[0]+','] + [list_genotypes[1]] + list_genotypes[2:])

#################################
######       MAIN       #########
#################################
 
# Read input files -----------
loci_names = [x.strip().split("\t") for x in open(input_loci_file).readlines()]
geno_file = [x.strip().split("\t") for x in open(input_geno_file).readlines()]

# reshape genotypes [-1012] to [0000,0101,0102,0202] -----------
genotypes_reshape = list(reshape_loci(x) for x in geno_file)

# mutate to pandas dataframe  -----------
table_genotypes = pd.DataFrame(genotypes_reshape)
table_genotypes = table_genotypes.set_axis([["IND","POP"] + ['_'.join(i) for i in loci_names]], axis=1)

# set dataframe index -----------
table_genotypes = table_genotypes.set_index("POP")
# reorder dataFrame rows by POP index -----------
table_genotypes = table_genotypes.sort_index()

# get list of dataframe idex  -----------
list_index = list(sorted(" ".join(x) for x in table_genotypes.index))

list_pops = sorted(set(list_index))

# Count and print population size  -----------
count_list = [[x,list_index.count(x)] for x in list_pops]
print("POP\tSIZE")
for element in range(len(count_list)):
    print(count_list[element][0] + "\t" + str(count_list[element][1]))


# Ouptu final genepop file  -----------
print("...writing output genepop file...")
with open(output_file, 'w') as f:
    f.write("Uncle_lobster\n")
    for locus in loci_names:
        f.write("".join(locus) +"\n")
    for i in range(0,len(list_pops)):
        f.write("Pop" + "\n")
        df = table_genotypes.filter(like=list_pops[i], axis=0)
        f.write(df.to_string(index=False, header=False) + "\n")